# Building Container

## 1. Instructions to build the image by stage

### 1.0 Generic way to build a docker imgage
```bash
docker build -t  <IMAGE_NAME>:<VERSION>  .
```

### 1.1 Build the required jar
```bash
mvn clean package
```


### 1.2.0 Set image name
```bash
export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd

# set the required image for the test 01-basic, 02-spring01, 03-spring02, 04-spring03...
export IMAGE_VERSION="01-basic"
```

### 1.2.1 Create the image for the project, execute all stages
```bash
docker build -t  $IMAGE_NAME:$IMAGE_VERSION .
```

## Use this for version with multi-stage images

### 1.3 Create the  test image, for run unit testing
```bash
export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd-test
export IMAGE_VERSION="SET_THE_SPECIIC_VERSION_TO_TEST"

docker build --target test -t $IMAGE_NAME:$IMAGE_VERSION .
```

### 1.4 Create the development image, for local development
```bash
export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd-dev
export IMAGE_VERSION="SET_THE_SPECIIC_VERSION_TO_TEST"

docker build --target development -t $IMAGE_NAME:$IMAGE_VERSION .
```

### 1.5 Create the build image, to execute package task
```bash
export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd-build
export IMAGE_VERSION="SET_THE_SPECIIC_VERSION_TO_TEST"

docker build --target build -t $IMAGE_NAME:$IMAGE_VERSION .
```

### 1.6 Create the image for production
```bash
export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd-prod
export IMAGE_VERSION="SET_THE_SPECIIC_VERSION_TO_TEST"

docker build --target production -t $IMAGE_NAME:$IMAGE_VERSION .
```

### 1.7 Create the image for builder production
```bash
export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd-builded-prod
export IMAGE_VERSION="SET_THE_SPECIIC_VERSION_TO_TEST"

docker build --target builded-production -t $IMAGE_NAME:$IMAGE_VERSION .
```

---

### 2.  Check and running your images

### 2.1 See the created images
```bash
docker images
```

### 2.2 Run locally your image
```bash
docker run -d -p 8080:8080 --name app01  $IMAGE_NAME:$IMAGE_VERSION
```

### 2.2.0 Open in a web browser the Swagger Interface http://localhost:8080/api/tasktracker/swagger-ui/index.html

### 2.2.1 Set endpoint
```bash
export APP_ENDPOINT="http://localhost:8080/api/tasktracker"
```


### 2.2.2 See the swagger definition
```bash
curl -X 'GET' "$APP_ENDPOINT/v3/api-docs" | jq
```

### 2.2.3 Check your API and GET all tasks
```bash
curl -X 'GET' "$APP_ENDPOINT/tasks" | jq
```

### 2.3 Go inside the container with shell
```bash
docker exec -it app01 sh
```

### 2.4 stop your container
```bash
docker stop app01
```

### 2.5 delete your container
```bash
docker rm app01
```

### 2.6 verify this container no longer exists
```bash
docker ps -a
```


---

## Resources, check other jdk images

- [Amazon Coreto](https://hub.docker.com/_/amazoncorretto/tags?page=1)
- [Container images for the Microsoft Build of OpenJDK](https://learn.microsoft.com/en-us/java/openjdk/containers)
- []()
- []()
- []()


