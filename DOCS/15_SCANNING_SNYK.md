

```bash
curl --compressed  https://static.snyk.io/cli/latest/snyk-linux  -o snyk
chmod +x ./snyk
sudo mv ./snyk /usr/local/bin/

```

### Set the environment variable on snyk
```bash
export SNYK_TOKEN="<SET_YOUR_SNYK_TOKEN>" 
```

### Scann your code SAST
```bash
snyk code test -d
# Create the sarif output file
snyk code test -d --sarif-file-output=snyk-sast.sarif
```

### Scan the opensource libraries SCA
```bash
snyk test -d    
snyk test -d --sarif-file-output=snyk-sca.sarif
```



### Scan the Container Image of your project
```bash
snyk container test  springboot-task-h2-gitlab-full-cicd-builded-prod:1.0 --file=Dockerfile  -d 
snyk container test  springboot-task-h2-gitlab-full-cicd-builded-prod:1.0 --file=Dockerfile  -d  --sarif-file-output=snyk-container.sarif
```

### Scan the IaC of your project
```bash
snyk iac test -d
snyk iac test -d --sarif-file-output=snyk-iac.sarif
```

### Monitor the project
```bash
snyk monitor  --project-name=spring-gitlab-cicd --org=$MY_ORGANIZATION
```




## References

### Snyk
- [snyk](https://snyk.io/)
- [Running scans](https://docs.snyk.io/scan-with-snyk/scanning-overview/running-scans)
- [Install or update the Snyk CLI](https://docs.snyk.io/snyk-cli/install-or-update-the-snyk-cli)
- [Snyk Open Source](https://snyk.io/product/open-source-security-management/)
- [Running scans](https://docs.snyk.io/scan-using-snyk/working-with-snyk-in-your-environment/running-scans)
- [Scan all unmanaged JAR files](https://docs.snyk.io/snyk-cli/scan-and-maintain-projects-using-the-cli/scan-all-unmanaged-jar-files)
- [Find Log4Shell vulnerabilities in your unmanaged and shaded jars with the Snyk CLI](https://snyk.io/blog/new-snyk-cli-command-finds-log4shell-in-unmanaged-undeclared-java-code/)
- [Snyk CLI for Snyk Code](https://docs.snyk.io/snyk-cli/scan-and-maintain-projects-using-the-cli/using-snyk-code-from-the-cli)
- [IaC security for devs and DevOps](https://snyk.io/product/infrastructure-as-code-security/)
- [Scan and fix security issues in Terraform files (current IaC)](https://docs.snyk.io/scan-with-snyk/scan-infrastructure/scan-your-iac-source-code/scan-terraform-files/scan-and-fix-security-issues-in-terraform-files-current-iac)
- [Black Duck vs Snyk comparison](https://www.peerspot.com/products/comparisons/black-duck_vs_snyk)


### IaC
- [Detect infrastructure drift and unmanaged resources with Snyk IaC](https://snyk.io/blog/detect-infrastructure-drift-unmanaged-resources-snyk-iac/)

### Defect Dojo
- [django-DefectDojo](https://github.com/DefectDojo/django-DefectDojo)
- [Installation](https://defectdojo.github.io/django-DefectDojo/getting_started/installation/)

### Java
- [How to switch between different java versions in Linux?](https://stackoverflow.com/questions/59856882/how-to-switch-between-different-java-versions-in-linux)