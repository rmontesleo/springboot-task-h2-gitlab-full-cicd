# Docker compose


### 1. Execute the compose
```bash
docker compose up -d
```

### 2. See the running containers
```bash
docker compose ps
```

### 3. See the logs containers
```bash
docker compose logs
```
### 3.1  See the logs containers with follow option
```bash
docker compose logs -f
```

### 4. Stop the compose
```bash
docker compose stop
```

### 5. Start again the compose
```bash
docker compose start
```

### 6.  Go inside you container
```bash
# with shell
docker exec -it springboot-task-tracker-h2-api-docker-container sh

# with bash
docker exec -it springboot-task-tracker-h2-api-docker-container bash
```

### 7. Delete the compose and delete the created volumenes ( -v )
```bash
docker compose down -v
```


