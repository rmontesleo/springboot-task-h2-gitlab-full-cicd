# Setup Docker Compose


### 1. Create your development files.  You will create the required environment files
```bash
touch docker-compose.override.yaml  .env_variables .env
```

### 2. In .env_variables set the variables for your project
```bash
user_name="Chanchito Feliz"
user_data="12345"
```

### 3. Edit the .env file, and add your required variables
```bash
TODO_API_VERSION="<SET_THE_VERSION_OF_THE_IMAGE_TO_TEST>"
```


### 4. Type the following content of docker-compose.override.yaml . This configuration is to run and test with docker compose

```yaml
version: '3.8'

services:
  api:
    build:
      context: .
      target: development
    environment:
      CHANCHITO: "Feliz"
    env_file:
      - .env_variables
      - .env
    volumes:
      - .:/app
    command: sh mvnw spring-boot:run
```