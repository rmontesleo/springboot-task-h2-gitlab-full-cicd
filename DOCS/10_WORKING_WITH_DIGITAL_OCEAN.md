

## Create Droplet

### Create Droplet with curl 
```bash
curl -X POST -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '$TOKEN'' \
    -d '{"name":"docker-ubuntu-s-1vcpu-1gb-sfo3-01",
        "size":"s-1vcpu-1gb",
        "region":"sfo3",
        "image":"docker-20-04",
        "vpc_uuid":"'$VPC_UUID'"}' \
    "https://api.digitalocean.com/v2/droplets"
```

### Create droplet with doctl cli
```bash
doctl compute droplet create \
    --image docker-20-04 \
    --size s-1vcpu-1gb \
    --region sfo3 \
    --vpc-uuid $VPC_UUID \
    docker-ubuntu-s-1vcpu-1gb-sfo3-01

```


## Rererences

- [Docker on Market Place in Digital Ocean](https://marketplace.digitalocean.com/apps/docker)