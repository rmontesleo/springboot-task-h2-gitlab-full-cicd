# Solving Errors

- [How to switch between different java versions in Linux?](https://stackoverflow.com/questions/59856882/how-to-switch-between-different-java-versions-in-linux)

- [ssh error while logging in using private key "loaded pubkey invalid format" and "error in libcrypto"?](https://unix.stackexchange.com/questions/577402/ssh-error-while-logging-in-using-private-key-loaded-pubkey-invalid-format-and)

- [Detect the container base image](https://docs.snyk.io/scan-using-snyk/snyk-container/use-snyk-container-from-the-web-ui/detect-the-container-base-image)

- [CVE-2023-5363](https://security.snyk.io/vuln/SNYK-ALPINE318-OPENSSL-6032386)
- [Remote Code Execution (RCE)](https://security.snyk.io/vuln/SNYK-JAVA-COMH2DATABASE-31685?utm_medium=Partner&utm_source=RedHat&utm_campaign=Code-Ready-Analytics-2020&utm_content=vuln%2FSNYK-JAVA-COMH2DATABASE-31685)

- [H2 Database Engine » 2.1.214](https://mvnrepository.com/artifact/com.h2database/h2/2.1.214)

- [eclipse-temurin:17-alpine](https://hub.docker.com/layers/library/eclipse-temurin/17-alpine/images/sha256-595dfc1148baa2a6d632cfa7ec5c793191290957551be4a46dde6d6e6c31d9c1?context=explore)


- [Docker endpoint for "default" not found](https://stackoverflow.com/questions/74804296/docker-endpoint-for-default-not-found)

- [Cannot run program mvn due to Permission Denied or No such file or directory errors when performing Bamboo Specs scan](https://confluence.atlassian.com/bamkb/cannot-run-program-mvn-due-to-permission-denied-or-no-such-file-or-directory-errors-when-performing-bamboo-specs-scan-1055002515.html)

- [What is the purpose of mvnw and mvnw.cmd files?](https://stackoverflow.com/questions/38723833/what-is-the-purpose-of-mvnw-and-mvnw-cmd-files)

- [Docker - failed to compute cache key: not found - runs fine in Visual Studio](https://stackoverflow.com/questions/66146088/docker-failed-to-compute-cache-key-not-found-runs-fine-in-visual-studio)