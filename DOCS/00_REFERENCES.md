# References


## Docker 
- [Manage access tokens](https://docs.docker.com/docker-hub/access-tokens/)
- [Ways to set environment variables in Compose](https://docs.docker.com/compose/environment-variables/set-environment-variables/)
- [](https://vsupalov.com/docker-arg-env-variable-guide/)
- [](https://phoenixnap.com/kb/docker-environment-variables)

## Baeldung
- [A Quick Guide to Maven Wrapper](https://www.baeldung.com/maven-wrapper)
- [](https://www.baeldung.com/ops/dockerfile-env-variable)
- [](https://www.baeldung.com/spring-cors)


## Spring, Java and API
- [](https://spring.io/guides/gs/rest-service-cors/)
- [](https://www.demo2s.com/java/spring-corsregistry-addmapping-string-pathpattern.html)
- [](https://codingnconcepts.com/spring-boot/spring-value-annotation/)
- [](https://www.programmergirl.com/convert-list-array-java)
- [](https://www.javatpoint.com/how-to-convert-string-to-string-array-in-java)
- [](https://refine.dev/blog/docker-build-args-and-env-vars/)


## Swagger
- [Swagger](https://swagger.io/docs/specification/media-types/)
- [Data Types](https://swagger.io/docs/specification/data-models/data-types/)



## Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.4/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#web)
* [Validation](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#io.validation)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#data.sql.jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Validation](https://spring.io/guides/gs/validating-form-input/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

