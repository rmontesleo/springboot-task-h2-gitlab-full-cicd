# Tag, Push and Pull Images to Docker Hub


### 1. Define your user name registry. In this case your docker hub username
```bash
export USER_REGISTRY_NAME="<SET_YOUR_DOCKER_USER_NAME>"
export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd-builded-prod
export IMAGE_VERSION="SET_THE_SPECIIC_VERSION_TO_TEST"
```

### 2. Tag your image
```bash
docker tag $IMAGE_NAME:$IMAGE_VERSION  $USER_REGISTRY_NAME/$IMAGE_NAME:$IMAGE_VERSION
```

### 3. Logout from docker user. To avoid push in a wrong registry
```bash
docker logout
```

### 4. Login in your docker hub account docker hub
```bash
docker login --username $USER_REGISTRY_NAME
```

### 5. Push the image to Docker hub

```bash
docker push  $USER_REGISTRY_NAME/$IMAGE_NAME:$IMAGE_VERSION
```