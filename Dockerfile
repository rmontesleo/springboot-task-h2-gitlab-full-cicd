# 1.  mvn clean package
# 2. export IMAGE_NAME=springboot-task-h2-gitlab-full-cicd
# 3. export IMAGE_VERSION=11-eclipse-temurin-v2
# 4. docker build -t  $IMAGE_NAME:$IMAGE_VERSION .
# 5. docker images
# 6. docker run -d -p 8080:8080 --name app01  $IMAGE_NAME:$IMAGE_VERSION

FROM eclipse-temurin:17-jdk-alpine as base
WORKDIR /app
COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN chmod +x  ./mvnw
RUN ./mvnw dependency:resolve
COPY src ./src

FROM base as test
CMD ["./mvnw", "test"]

FROM base as development
CMD ["./mvnw", "spring-boot:run"]


FROM base as build
RUN ./mvnw package

FROM eclipse-temurin:17-jre-alpine as production
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
EXPOSE 8080
COPY --from=build /app/target/task-tracker.jar /home/spring/task-tracker.jar
ENTRYPOINT ["java", "-jar", "/home/spring/task-tracker.jar" ]


FROM eclipse-temurin:17-jre-alpine as builded-production
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
EXPOSE 8080
COPY target/*.jar /home/spring/task-tracker.jar
ENTRYPOINT ["java", "-jar", "/home/spring/task-tracker.jar" ]