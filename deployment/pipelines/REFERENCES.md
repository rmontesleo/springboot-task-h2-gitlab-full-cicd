# GitLab References

## Important references (Romano Roth)
 -[GitLab: Build a DevSecOps Pipeline](https://www.youtube.com/playlist?list=PLrsbMazVPK_qhf3ahA_zRPlwBaGGhSu2P)
- [GitLabDevSecOpsPipeline](https://gitlab.com/romano_roth/gitlabdevsecopspipeline)


## GitLab Security
- [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)


## GitLab Documentation
- [keyword reference](https://docs.gitlab.com/ee/ci/yaml/index.html#when-manual)
- [Choose when to run jobs](https://docs.gitlab.com/ee/ci/jobs/job_control.html)
- [Deployment approvals](https://docs.gitlab.com/ee/ci/environments/deployment_approvals.html)


## About GitLab
- [Compare features](https://about.gitlab.com/pricing/feature-comparison/)
- [How to use manual jobs with `needs:` relationships](https://about.gitlab.com/blog/2021/05/20/dag-manual-fix/)

## Solve Issues
- [How to define a GitLab CI job to depend on either one or one another previous job?](https://stackoverflow.com/questions/68877379/how-to-define-a-gitlab-ci-job-to-depend-on-either-one-or-one-another-previous-jo)
- [How do I establish manual stages in Gitlab CI?](https://stackoverflow.com/questions/31904686/how-do-i-establish-manual-stages-in-gitlab-ci)
- [Manual approval in GitLab CI deployment pipeline](https://stackoverflow.com/questions/58338356/manual-approval-in-gitlab-ci-deployment-pipeline)

## Many References