
resource "linode_lke_cluster" "spring_h2_lke_cluster" {
    k8s_version="1.26"
    label="spring_h2_lke_cluster"
    region="us-east"
    tags=["spring_h2_lke_cluster"]
    pool {
        type  = "g6-standard-1"
        count = 3

    }
}