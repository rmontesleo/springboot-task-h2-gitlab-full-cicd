#!/bin/bash

echo "##############################################################"
echo "Create Service Account (Dedicated User)"
echo "Create the Role (Grants)"
echo "Create the RoleBinding (Link the Service Account and the Role)"
echo "##############################################################"

echo "##############################################################"
echo "Setup Variables"
echo "##############################################################"
export KUBECONFIG=kubeconfig.yaml
NAMESPACE="spring-task-h2"
SERVICE_ACCOUNT="cicd-sa"
ROLE_FILE="cicd-role.yaml"
ROLE="cicd-role"
ROLE_BINDING="cicd-rb"
DECODED_TOKEN_FILE="decoded_token.txt"


echo "##############################################################"
echo "Verify you can connect with the k8s cluster"
echo "##############################################################"
kubectl get nodes -o wide

echo "##############################################################"
echo "Create namespace $NAMESPACE"
echo "##############################################################"
kubectl create namespace $NAMESPACE
kubectl get namespace


echo "##############################################################"
echo "create the service account  $SERVICE_ACCOUNT (imperative)"
echo "##############################################################"
kubectl create serviceaccount $SERVICE_ACCOUNT --namespace=$NAMESPACE

kubectl get serviceaccount -n $NAMESPACE


echo "##############################################################"
echo "Create the role $ROLE (declarative) with the file $ROLE_FILE"
echo "##############################################################"
kubectl apply -f $ROLE_FILE

echo "##############################################################"
echo "Create role binding $ROLE_BINDING (link service account and roles)"
echo "##############################################################"
kubectl create rolebinding $ROLE_BINDING \
--role=$ROLE \
--serviceaccount=$NAMESPACE:$SERVICE_ACCOUNT \
--namespace=$NAMESPACE

echo "##############################################################"
echo "Get the secret name of the service account"
echo "##############################################################"
secret_name_of_service_account=$(kubectl get serviceaccount $SERVICE_ACCOUNT -n $NAMESPACE -o json | jq .secrets[0].name -r)
echo "The secret name of service account is: $secret_name_of_service_account"

echo "##############################################################"
echo "Get the the token from the secret of the service account"
echo "##############################################################"
token=$(kubectl get secret $secret_name_of_service_account -n $NAMESPACE -o json | jq .data.token -r)
echo "The token is $token"

echo "##############################################################"
echo "Decode the token"
echo "##############################################################"
decoded_token=$(echo $token | base64 -d)
echo $decoded_token > $DECODED_TOKEN_FILE
echo "Decoded token is created on file $DECODED_TOKEN_FILE"

echo "##############################################################"
echo "Create the cicd file: cicd-$KUBECONFIG "
echo "##############################################################"
cp $KUBECONFIG cicd-$KUBECONFIG


echo "##############################################################"
echo "Replace token by the decoded token "
echo "##############################################################"

echo "... pending this task, right now replace it manually"

echo "##############################################################"
echo "End of setting RBAC..."
echo "##############################################################"
   