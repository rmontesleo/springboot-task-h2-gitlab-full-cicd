
resource "local_file" "k8s_config" {
    content = "${nonsensitive(base64decode(linode_lke_cluster.spring_h2_lke_cluster.kubeconfig))}"
    filename = "kubeconfig.yaml"
    file_permission = "0400"
}