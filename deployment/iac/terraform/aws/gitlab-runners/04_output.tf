#############################################################
## GitLab Runners
#############################################################

output "shell_runner_ip_addr" {
  value = aws_instance.shell-remote-runner-node.public_ip
}

output "shell_runner_public_dns" {
  value = aws_instance.shell-remote-runner-node.public_dns
}


output "docker_01_runner_ip_addr" {
  value = aws_instance.docker-remote-runner-node01.public_ip
}

output "docker_01_runner_public_dns" {
  value = aws_instance.docker-remote-runner-node01.public_dns
}

