#############################################################
## EC2 Configuration
#############################################################


resource "aws_security_group" "gitlab_runner_security_group" {
  name   = "gitlab_runner_security_group"
  vpc_id = aws_vpc.runners_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_key_pair" "gitlab_cicd_key" {
  key_name   = "nana_cicd_key"
  public_key = file("~/.ssh/aws/demos/gitlab/cicd_key.pub")
}



#EC2 Instance
resource "aws_instance" "shell-remote-runner-node" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.gitlab_cicd_key.id
  subnet_id                   = aws_subnet.runners_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_security_group.id]
  user_data = <<-EOF
    #!/bin/bash
  
    # Install in the Ubuntu/debian/mint server
    sudo apt-get update
    sudo apt-get -y install jq vim curl
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
    sudo apt-get install gitlab-runner
  
    # The check the runner
    sudo gitlab-runner -version
    sudo gitlab-runner status
  
    # Register
    sudo gitlab-runner register --non-interactive \
    --url https://gitlab.com \
    --registration-token ${var.spring_h2_app_gitlab_runner_registration_token} \
    --description "shell-remote" \
    --tag-list "ec2,shell,remote" \
    --maintenance-note "Verify the notes" \
    --executor "shell"
  
    # start runner
    sudo gitlab-runner start

    # Install trivy
    wget https://github.com/aquasecurity/trivy/releases/download/v0.45.1/trivy_0.45.1_Linux-64bit.deb
    sudo dpkg -i trivy_0.45.1_Linux-64bit.deb

    # Install snyc
    curl https://static.snyk.io/cli/latest/snyk-linux -o snyk
    chmod +x ./snyk
    sudo mv ./snyk /usr/local/bin/

    # Install kubectl
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
    echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

  
    # Configure Docker
    sudo apt install docker.io -y
    sudo usermod -aG docker $USER
    sudo usermod -aG docker gitlab-runner
    sudo usermod -aG docker ubuntu
    newgrp docker 
  
    EOF

  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "shell-remote-runner-node"
    Project = "cicd-project-tag"
  }

}


#EC2 Instance
resource "aws_instance" "docker-remote-runner-node01" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.gitlab_cicd_key.id
  subnet_id                   = aws_subnet.runners_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_security_group.id]
  user_data = <<-EOF
    #!/bin/bash

    # Install in the Ubuntu/debian/mint server
    sudo apt-get update
    sudo apt-get -y install jq vim curl
    
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
    sudo apt-get install gitlab-runner

    # The check the runner
    sudo gitlab-runner -version
    sudo gitlab-runner status

    # Register
    sudo gitlab-runner register --non-interactive \
    --url https://gitlab.com \
    --registration-token ${var.spring_h2_app_gitlab_runner_registration_token} \
    --description "docker-remote-01" \
    --tag-list "ec2,docker,remote" \
    --maintenance-note "Verify the notes" \
    --executor "docker" \
    --docker-image "alpine:3.15.1" 


    # start runner
    sudo gitlab-runner start

    # Configure Docker
    sudo apt install docker.io -y
    sudo usermod -aG docker $USER
    sudo usermod -aG docker ubuntu
    newgrp docker

    EOF

  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "docker-remote-runner-node01"
    Project = "cicd-project-tag"
  }

}
