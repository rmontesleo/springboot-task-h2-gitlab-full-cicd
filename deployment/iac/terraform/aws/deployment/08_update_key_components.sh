#!/bin/bash

export VARIABLES_ENDPOINT="https://gitlab.com/api/v4/projects/50639992/variables"
echo "VARIABLES_ENDPOINT is $VARIABLES_ENDPOINT"

#############################################################
## Update GITLAB_USER
#############################################################
curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/GITLAB_USER" --form "value=$GITLAB_USER" | jq

#############################################################
## Update KUBE_CONFIG
#############################################################
KUBE_CONFIG=$(cat ../../linode/cicd-kubeconfig.yaml)

curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/KUBE_CONFIG" --form "value=$KUBE_CONFIG" | jq

#############################################################
## Update REGISTRY_TOKEN
#############################################################
REGISTRY_TOKEN=$GITLAB_DEVELOPMENT_TOKEN

curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/REGISTRY_TOKEN" --form "value=$REGISTRY_TOKEN" | jq


#############################################################
## Update SNYK_TOKEN
#############################################################
curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/SNYK_TOKEN" --form "value=$SNYK_TOKEN" | jq

#############################################################
## Update SSH_PRIVATE_KEY
#############################################################
#TODO: Verify if this require a extraprocess to keep each line of the ssh key
SSH_PRIVATE_KEY=$(cat ~/.ssh/aws/demos/gitlab/cicd_key)
curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/SSH_PRIVATE_KEY" --form "value=$SSH_PRIVATE_KEY" | jq

