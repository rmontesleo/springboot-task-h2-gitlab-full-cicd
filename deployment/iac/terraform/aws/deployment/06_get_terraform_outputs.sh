#!/bin/bash

#############################################################
## Update Development environment
#############################################################

DEV_PORT=8080
DEV_DNS=$(terraform output -raw dev_node_public_dns)
DEV_ENDPOINT="http://$DEV_DNS:$DEV_PORT"
DEV_SERVER_HOST=$(terraform output -raw dev_node_ip_addr)

echo "DEV_PORT is $DEV_PORT"
echo "DEV_DNS is $DEV_DNS"
echo "DEV_ENDPOINT is $DEV_ENDPOINT"
echo "DEV_SERVER_HOST is $DEV_SERVER_HOST"
echo ""

#############################################################
## Update Stage Environment
#############################################################

STAGING_PORT=8080
STAGING_DNS=$(terraform output -raw stage_node_public_dns)
STAGING_ENDPOINT="http://$STAGING_DNS:$STAGING_PORT"
STAGING_SERVER_HOST=$(terraform output -raw stage_node_ip_addr)

echo "STAGING_PORT is $STAGING_PORT"
echo "STAGING_DNS is $STAGING_DNS"
echo "STAGING_ENDPOINT is $STAGING_ENDPOINT"
echo "STAGING_SERVER_HOST is $STAGING_SERVER_HOST"
echo ""

#############################################################
## Update Production Environment
#############################################################

PROD_PORT=8080
PROD_DNS=$(terraform output -raw prod_node_public_dns)
PROD_ENDPOINT="http://$PROD_DNS:$PROD_PORT"
PROD_SERVER_HOST=$(terraform output -raw prod_node_ip_addr)

echo "PROD_PORT is $PROD_PORT"
echo "PROD_DNS is $PROD_DNS"
echo "PROD_ENDPOINT is $PROD_ENDPOINT"
echo "PROD_SERVER_HOST is $PROD_SERVER_HOST"
echo ""