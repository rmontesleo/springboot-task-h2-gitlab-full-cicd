#!/bin/bash

export VARIABLES_ENDPOINT="https://gitlab.com/api/v4/projects/50639992/variables"
echo "VARIABLES_ENDPOINT is $VARIABLES_ENDPOINT"


#############################################################
## Update Development environment
#############################################################

DEV_PORT=8080
DEV_DNS=$(terraform output -raw dev_node_public_dns)
DEV_ENDPOINT="http://$DEV_DNS:$DEV_PORT"
DEV_SERVER_HOST=$(terraform output -raw dev_node_ip_addr)

# update variable
curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/DEV_ENDPOINT" --form "value=$DEV_ENDPOINT" | jq

curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/DEV_SERVER_HOST" --form "value=$DEV_SERVER_HOST" | jq



#############################################################
## Update Stage Environment
#############################################################

STAGING_PORT=8080
STAGING_DNS=$(terraform output -raw stage_node_public_dns)
STAGING_ENDPOINT="http://$STAGING_DNS:$STAGING_PORT"
STAGING_SERVER_HOST=$(terraform output -raw stage_node_ip_addr)

# update variable
curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/STAGING_ENDPOINT" --form "value=$STAGING_ENDPOINT" | jq

curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/STAGING_SERVER_HOST" --form "value=$STAGING_SERVER_HOST" | jq

#############################################################
## Update Production Environment
#############################################################

PROD_PORT=8080
PROD_DNS=$(terraform output -raw prod_node_public_dns)
PROD_ENDPOINT="http://$PROD_DNS:$PROD_PORT"
PROD_SERVER_HOST=$(terraform output -raw prod_node_ip_addr)

# update variable
curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/PROD_ENDPOINT" --form "value=$PROD_ENDPOINT" | jq

curl --request PUT --header "PRIVATE-TOKEN: $SPRING_H2_FULL_CICD_GITLAB_TOKEN" \
"$VARIABLES_ENDPOINT/PROD_SERVER_HOST" --form "value=$PROD_SERVER_HOST" | jq


#############################################################
## Update Production Environment
#############################################################

# get all variables
#export NEW_VARIABLES=$(curl --header "PRIVATE-TOKEN:  $SPRING_H2_FULL_CICD_GITLAB_TOKEN" "$VARIABLES_ENDPOINT" )
#echo $NEW_VARIABLES | jq

