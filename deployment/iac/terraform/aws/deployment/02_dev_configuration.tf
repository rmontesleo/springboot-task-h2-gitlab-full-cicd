#############################################################
## Route table configuration for Development environment
#############################################################

# Item 03
resource "aws_route_table" "dev_rt" {
  vpc_id = aws_vpc.dev_vpc.id
  tags = {
    Name    = "cicd-dev-route-table"
    Project = "cicd-gitlab-runners"
  }
}

# Item 04
resource "aws_route_table_association" "dev_public_assoc" {
  subnet_id      = aws_subnet.dev_subnet.id
  route_table_id = aws_route_table.dev_rt.id
}


# Item 06
resource "aws_route" "dev_public_route" {
  route_table_id         = aws_route_table.dev_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.dev_igw.id
}




#############################################################
## EC2 Configuration
#############################################################


resource "aws_security_group" "dev_security_group" {
  name   = "dev_security_group"
  vpc_id = aws_vpc.dev_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_key_pair" "dev_cicd_key" {
  key_name   = "dev_cicd_key"
  public_key = file("~/.ssh/aws/demos/gitlab/cicd_key.pub")
}

#EC2 Instance
resource "aws_instance" "dev-node" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.dev_cicd_key.id
  subnet_id                   = aws_subnet.dev_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.dev_security_group.id]

  user_data = <<-EOF
    #!/bin/bash

    # Update the instance
    sudo apt-get update

    # Install and Configure Docker
    sudo apt install docker.io -y
    sudo usermod -aG docker $USER
    sudo usermod -aG docker ubuntu
    sudo apt  install docker-compose -y
    
    newgrp docker
    
    EOF

  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "dev-node"
    Project = "cicd-project-tag"
  }

}
