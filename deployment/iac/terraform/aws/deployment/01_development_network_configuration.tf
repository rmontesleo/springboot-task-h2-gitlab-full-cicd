#############################################################
## Network configuration
#############################################################


#############################################################
## VPC
#############################################################
resource "aws_vpc" "dev_vpc" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name    = "cicd-d3-vpc"
    Project = "cicd-gitlab-runners"
  }
}

#############################################################
## Internet Gateway
#############################################################
resource "aws_internet_gateway" "dev_igw" {
  vpc_id = aws_vpc.dev_vpc.id
  tags = {
    Name    = "dev_igw"
    Project = "cicd-gitlab-runners"
  }
}

#############################################################
## Subnet
#############################################################

# Subnet for Development
resource "aws_subnet" "dev_subnet" {
  vpc_id                  = aws_vpc.dev_vpc.id
  cidr_block              = "10.1.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name    = "cicd-dev-subnet"
    Project = "cicd-gitlab-runners"
  }
}

# Subnet for Stage
resource "aws_subnet" "stage_subnet" {
  vpc_id                  = aws_vpc.dev_vpc.id
  cidr_block              = "10.1.3.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name    = "cicd-stage-subnet"
    Project = "cicd-gitlab-runners"
  }
}

