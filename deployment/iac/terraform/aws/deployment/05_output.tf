#############################################################
## Dev
#############################################################

output "dev_node_ip_addr" {
  value = aws_instance.dev-node.public_ip
}

output "dev_node_public_dns" {
  value = aws_instance.dev-node.public_dns
}

#############################################################
## Stage
#############################################################

output "stage_node_ip_addr" {
  value = aws_instance.stage-node.public_ip
}

output "stage_node_public_dns" {
  value = aws_instance.stage-node.public_dns
}


#############################################################
## Production
#############################################################

output "prod_node_ip_addr" {
  value = aws_instance.prod-node.public_ip
}

output "prod_node_public_dns" {
  value = aws_instance.prod-node.public_dns
}

