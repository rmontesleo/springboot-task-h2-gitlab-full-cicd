#############################################################
## Network configuration
#############################################################

resource "aws_vpc" "prod_vpc" {
  cidr_block           = "10.2.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name    = "cicd-prod-vpc"
    Project = "cicd-gitlab-runners"
  }
}

resource "aws_subnet" "prod_subnet" {
  vpc_id                  = aws_vpc.prod_vpc.id
  cidr_block              = "10.2.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name    = "cicd-prod-subnet"
    Project = "cicd-gitlab-runners"
  }
}

# Item 05
resource "aws_internet_gateway" "prod_igw" {
  vpc_id = aws_vpc.prod_vpc.id
  tags = {
    Name    = "prod_igw"
    Project = "cicd-gitlab-runners"
  }
}




#############################################################
## Route Table configuration
#############################################################

# Item 03
resource "aws_route_table" "prod_rt" {
  vpc_id = aws_vpc.prod_vpc.id
  tags = {
    Name    = "cicd-prod-route-table"
    Project = "cicd-gitlab-runners"
  }
}

# Item 04
resource "aws_route_table_association" "prod_public_assoc" {
  subnet_id      = aws_subnet.prod_subnet.id
  route_table_id = aws_route_table.prod_rt.id
}



# Item 06
resource "aws_route" "prod_public_route" {
  route_table_id         = aws_route_table.prod_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.prod_igw.id
}



#############################################################
## EC2 Configuration
#############################################################


resource "aws_security_group" "prod_security_group" {
  name   = "prod_security_group"
  description = "Security group for production environment"
  vpc_id = aws_vpc.prod_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_key_pair" "prod_cicd_key" {
  key_name   = "prod_cicd_key"
  public_key = file("~/.ssh/aws/demos/gitlab/cicd_key.pub")
}

#EC2 Instance
resource "aws_instance" "prod-node" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.prod_cicd_key.id
  subnet_id                   = aws_subnet.prod_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.prod_security_group.id]

  user_data = <<-EOF
    #!/bin/bash

    # Update the instance
    sudo apt-get update

    # Install and Configure Docker
    sudo apt install docker.io -y
    sudo usermod -aG docker $USER
    sudo usermod -aG docker ubuntu
    sudo apt  install docker-compose -y
    
    newgrp docker
    
    EOF

  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "prod-node"
    Project = "cicd-project-tag"
  }

}
